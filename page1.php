<!DOCTYPE html>

<head>
    <link rel="stylesheet" href="CSS/stylesheet.css">
    <link rel="stylesheet" href="CSS/page1.css">
    <meta charset="utf-8">
    <meta name="author" content="Adrian Knorr">
    <meta name="keywords" content="HTML, CSS, JS">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <title>My Website</title>
</head>

<body>
    <header>
        <nav>
            <ul class="flex-container">
                <li class="nav-li"><a href="index.html">Home</a></li>
                <li class="nav-li active"><a href="page1.php">Page1</a></li>
                <li class="nav-li"><a href="page2.html">Page2</a></li>
                <li class="nav-li"><a href="page3.html">Page3</a></li>
                <li class="nav-li"><a href="page4.html">Page4</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <form action="" method="">
            <label for="name">*Your name:</label>
            <input id="name" type="text" required>

            <label for="email">*Your email:</label>
            <input id="email" type="email" required>

            <div id="gender">
                <label for="gen1">Gender: Male</label>
                <input name="gender" id="gen1" type="radio">
                <label for="gen2"> Gender: Female</label>
                <input name="gender" id="gen2" type="radio">
            </div>

            <div id="notification">
                <label for="not">Receive notifications</label>
                <input id="not" type="checkbox">
            </div>

            <label for="msg">Your message:</label>
            <textarea id="msg" required></textarea>
            <button type="submit">Submit</button>
        </form>
    </main>

    <footer>
        <div id="footer-wrapper" class="flex-container">
            <div class="left">
                Adrian Knorr<br>Kaiserstr. 16<br>75512 Berlin<br>Germany - Berlin
            </div>
            <div class="mid">

            </div>
            <div class="right">
                <ul>
                    <li>IT System Administration</li>
                    <li>Frontend Development</li>
                    <li>SBC Engineer</li>
                </ul>
            </div>
        </div>
    </footer>
</body>

</html>